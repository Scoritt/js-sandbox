// object literal
const finOper = {
    amount: 105.3,
    date: '21 Nov 2017',
    classifier: 'food'
};

console.log(finOper);
    // { amount: 105.3, date: '21 Nov 2017', classifier: 'food' }

const finOper2 = finOper;

finOper2.classifier = 'transport';

console.log(finOper);
    // { amount: 105.3, date: '21 Nov 2017', classifier: 'food' }

// accessing properties using bracket notation
console.log(finOper['date']);
    // 21 Nov 2017

// check member presence
console.log('person' in finOper2)
    // false

finOper2['person'] = 'Max';
console.log('person' in finOper2)
    // true