function myHello() {
    console.log('myHello function')
}
console.log(myHello.name);


// not recommended
const hi =  new Function('console.log("Hi");');
console.log(hi());

// arguments
const args = function() {
    console.log('Length: ' + arguments.length);
    console.log(arguments);
    return arguments;
};
const a = args(1, 2, 'three', NaN, undefined);
console.log(a);                 // { '0': 1, '1': 2, '2': 'three', '3': NaN, '4': undefined }

// default parameters
function divisor(value, divisor = 2) {
    return value / divisor;
}
console.log(divisor(100));      // 50
console.log(divisor(100, 4));   // 25
console.log(divisor(100, 0));   // Infinity

// arrow functions
const mySum = (x, y) => x+y;
const justLog = () => console.log('=== Log ===');
console.log(mySum(3, 2));       // 5
justLog();                      // === Log ===


console.log(typeof(mySum));     // function


console.log([1, 2, 3].filter(x => x > 2));  // [ 3 ]
console.log([1, 2, 3].map(x => x * 10));    // [ 10, 20, 30 ]
console.log([1, 2, 3].reduce((a, b) => a + b)); // 6

