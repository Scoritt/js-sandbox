/**
 *
 * @param initial Initial payment
 * @param additional Additional payment at the end of each period
 * @param rate Yearly rate in absolute value (e.g. 0.046)
 * @param periodNumber No of periods
 * @param period Number of addition interest per year
 * @returns {number} total amount at the end
 */
function cinterest(initial, additional, rate, periodNumber, period = 12) {
    const i = 1 + rate / period;
    return initial*Math.pow(i, periodNumber) + additional*((1-Math.pow(i, periodNumber-1))/(1-i)-1);
}

function periodIter(endPeriod) {

    console.log('| period | amount | monthly | inflation included |')

    for(let i = 1; i<=endPeriod; i++) {
        const s = cinterest(200000, 130000, 0.046, i);

        let sp = '| ' + i + ' | ' + s + ' | ' + s*0.046/12 + ' | ' + (s*0.046/12)*Math.pow(1 - (0.07/12), i) + ' |';

        console.log(sp);
    }
}

periodIter(120);
